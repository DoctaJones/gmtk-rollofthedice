extends CanvasLayer


signal game_restarted


func _on_Control_gui_input(event):
    if event is InputEventMouseButton && event.pressed:
        emit_signal("game_restarted")
        queue_free()
