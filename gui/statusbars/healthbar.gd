# This is the healthbar that hovers over the heads of things in the game

extends TextureProgress


var bar_red = preload("res://gui/statusbars/healthbar_small_red.png")
var bar_green = preload("res://gui/statusbars/healthbar_small_green.png")
var bar_yellow = preload("res://gui/statusbars/healthbar_small_yellow.png")

# Called when the node enters the scene tree for the first time.
func _ready():
    self.texture_progress = bar_green

func setup(max_value):
    self.max_value = max_value
    self.value = max_value
    
    self.get_parent().connect("health_change", self, "on_health_change")


func on_health_change(health_change):
    self.value = self.value + health_change

    if self.value < self.max_value * 0.35:
        self.texture_progress = bar_red
    elif self.value < self.max_value * 0.7:
        self.texture_progress = bar_yellow
    else:
        self.texture_progress = bar_green
