extends CanvasLayer


signal game_started


func _on_Control_gui_input(event):
    if event is InputEventMouseButton && event.pressed:
        emit_signal("game_started")
        queue_free()
