extends Node

var hud = preload("res://gui/hud/hud.tscn")
var main_menu = preload("res://gui/main_menu/main_menu.tscn")
var death_screen = preload("res://gui/death_screen/death_screen.tscn")

var bar_red = preload("res://gui/statusbars/healthbar_red.png")
var bar_green = preload("res://gui/statusbars/healthbar_green.png")
var bar_yellow = preload("res://gui/statusbars/healthbar_yellow.png")

var player_node = null

func _ready():
    var main_menu_instance = main_menu.instance()
    main_menu_instance.connect('game_started', self, 'on_game_start')
    add_child(main_menu_instance)


func on_game_start():
    load_level('level_01')
    spawn_player()
    load_hud()


func spawn_player():
    var player = load("res://player/player.tscn")
    player_node = player.instance()
    var player_spawn = $level.get_node("PlayerSpawn").position

    player_node.spawn(player_spawn)
    add_child(player_node)

    player_node.connect("player_health_changed", self, "update_healthbar")
    player_node.connect("player_died", self, "handle_player_death")
    player_node.connect("player_luck_changed", self, "toggle_bad_luck_message")


func load_level(level_name):
    var level = load("res://levels/%s/level.tscn" % [level_name])
    var level_instance = level.instance()

    add_child(level_instance)


func load_hud():
    var hud_instance = hud.instance()
    add_child(hud_instance)

    $HUD/healthlabel.set_text(str(player_node.stats['health']))
    $HUD/healthbar.texture_progress = bar_green
    $HUD/healthbar.max_value = player_node.stats['health']
    $HUD/healthbar.value = player_node.stats['health']
    
    $HUD/badluckmessage.visible = false


func update_healthbar():
    $HUD/healthlabel.set_text(str(player_node.stats['health']))

    if player_node.stats['health'] < $HUD/healthbar.max_value * 0.35:
        $HUD/healthbar.texture_progress = bar_red
    elif player_node.stats['health'] < $HUD/healthbar.max_value * 0.7:
        $HUD/healthbar.texture_progress = bar_yellow
    else:
        $HUD/healthbar.texture_progress = bar_green

    $HUD/healthbar.value = player_node.stats['health']


func toggle_bad_luck_message(bad_luck):
    $HUD/badluckmessage.visible = bad_luck


func handle_restart():
    on_game_start()


func handle_player_death():
    for child in get_children():
        remove_child(child)
        child.queue_free()

    var death_screen_instance = death_screen.instance()
    death_screen_instance.connect('game_restarted', self, 'handle_restart')
    add_child(death_screen_instance)
