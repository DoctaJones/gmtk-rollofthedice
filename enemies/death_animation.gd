extends Node2D

func _ready():
    $AnimationPlayer.play("death")


func setup(spawn_position):
    position = spawn_position

func _on_AnimationPlayer_animation_finished(anim_name):
    queue_free()
