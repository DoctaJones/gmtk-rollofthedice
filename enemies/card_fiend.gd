extends KinematicBody2D

export (int) var speed = 25
export (int) var health = 4

var category = 'enemy'
var velocity = Vector2.ZERO

signal health_change(health_change)
signal enemy_death(self_node)

var player_node = null

var death_animation = preload("res://enemies/death_animation.tscn")
var hit_sound = preload("res://enemies/card_fiend_hit_sound.tscn")

func _ready():
    player_node = get_tree().get_current_scene().get_node('player')
    $healthbar.setup(health)


func _physics_process(delta):
    if player_node:
        var player_position = player_node.position

        velocity = position.direction_to(player_position) * speed
        var collision = move_and_collide(velocity * delta)

        if collision:
            if collision.collider.name == "player":
                collision.collider.hit('contact', 1)
            else:
                velocity = velocity.slide(collision.normal)

    if health <= 0:
        emit_signal("enemy_death", self)
        die()


func spawn(spawn_position):
    position = spawn_position


func hit(type, damage):
    health = health - damage
    emit_signal('health_change', -damage)

    var hit_sound_instance = hit_sound.instance()
    hit_sound_instance.connect('finished', hit_sound_instance, 'queue_free')
    get_parent().add_child(hit_sound_instance)


func die():
    var death_animation_instance = death_animation.instance()
    death_animation_instance.setup(position)
    get_parent().add_child(death_animation_instance)
    queue_free()
