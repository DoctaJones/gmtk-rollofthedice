extends KinematicBody2D

var enemy = preload("res://enemies/card_fiend.tscn")

var velocity = Vector2.ZERO
var speed =  50

var player_node

var moving = true
var spawn_points

onready var spawn_points_left = $spawn_points_left
onready var spawn_points_right = $spawn_points_left


func _ready():
    player_node = get_tree().get_current_scene().get_node('player')

    if player_node.position.x < position.x:
        $Sprite.flip_h = false
    else:
        $Sprite.flip_h = true
    


func setup(target, spawn_position):
    position = spawn_position

    var vec_to_target = target - position
    rotation = vec_to_target.angle()


func _physics_process(delta):
    if player_node:
        var player_position = player_node.position
        if moving and abs(player_position.x - position.x) > 5:
            if player_position.x < position.x:
                $Sprite.flip_h = false
            else:
                $Sprite.flip_h = true

            velocity = position.direction_to(player_position) * speed
            move_and_collide(velocity * delta)


func spawn_enemies():
    var spawn_points

    if $Sprite.flip_h:
        spawn_points = spawn_points_right
    else:
        spawn_points = spawn_points_left

    for spawn_point in spawn_points.get_children():
        var enemy_instance = enemy.instance()
        enemy_instance.spawn(spawn_point.global_position)
        get_parent().add_child(enemy_instance)


func spawn(hand_spawn):
    position = hand_spawn


func play_hand_animation(direction):
    if direction == 'down':
        $place_hand.play("touch_table")
        yield(get_node("place_hand"), 'animation_finished')
        spawn_enemies()


func _on_spawn_delay_timeout():
    moving = false
    velocity = Vector2.ZERO

    $place_hand.play("touch_table")
    yield(get_node("place_hand"), 'animation_finished')
    spawn_enemies()

    $place_hand.play_backwards("touch_table")
    yield(get_node("place_hand"), 'animation_finished')
    queue_free()
