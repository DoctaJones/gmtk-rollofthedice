extends Area2D

var player_node = null

func _ready():
    $spotlight_lifespawn_timer.start()


func spawn(spawn_position):
    position = spawn_position


func _on_spotlight_body_entered(body):
    if body.name == 'player':
        body.bad_luck = false
        body.good_luck = true


func _on_spotlight_lifespawn_timer_timeout():
    queue_free()
