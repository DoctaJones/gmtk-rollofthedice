extends Node2D


onready var hand = preload("res://spawners/hand.tscn")
onready var spotlight = preload("res://levels/resources/spotlight.tscn")

onready var hand_placements = $hand_placements
onready var spotlight_placements = $spotlight_placements

#var rng = RandomNumberGenerator.new()
var hand_placement_id = 0
var spotlight_placement_id = 0


func place_hand():
    var hand_instance = hand.instance()
    var spawn_point = hand_placements.get_child(hand_placement_id).position

    hand_instance.spawn(spawn_point)
    add_child(hand_instance)

    hand_placement_id += 1
    if hand_placement_id == hand_placements.get_child_count():
        hand_placement_id = 0


func place_spotlight():
    var spotlight_instance = spotlight.instance()
    var spawn_point = spotlight_placements.get_child(spotlight_placement_id).position

    spotlight_instance.spawn(spawn_point)
    add_child(spotlight_instance)

    spotlight_placement_id += 1
    if spotlight_placement_id == spotlight_placements.get_child_count():
        spotlight_placement_id = 0


func _on_hand_placement_timer_timeout():
    place_hand()


func _on_spotlight_placement_timer_timeout():
    place_spotlight()

