extends KinematicBody2D

var coin = preload("res://player/projectiles/coin/coin.tscn")
var moneybomb_spawn_sound = preload("res://player/projectiles/moneybomb/moneybomb_spawn_sound.tscn")
var moneybomb_explode_sound = preload("res://player/projectiles/moneybomb/moneybomb_explode.tscn")
#var moneybomb_hit_sound = preload("res://player/projectiles/moneybomb/moneybomb_hit_sound.tscn")

onready var bag_center = $bag_center

var velocity = Vector2.ZERO
var speed = 100
var fire_rate = 0.4

var spawn_position
var vec_to_target

func _ready():
    $moneybomb_lifespan_timer.start()
    
    var moneybomb_spawn_sound_instance = moneybomb_spawn_sound.instance()
    moneybomb_spawn_sound_instance.connect('finished', moneybomb_spawn_sound_instance, 'queue_free')
    get_parent().add_child(moneybomb_spawn_sound_instance)

    velocity = Vector2(1, 0).rotated(rotation) * speed


func setup(target, projectile_spawn_position):
    position = projectile_spawn_position

    vec_to_target = target - position
    rotation = vec_to_target.angle()


func _physics_process(delta):
    var collision = move_and_collide(velocity * delta)

    if Input.is_action_just_pressed("explode"):
        explode()

    if collision:
        if collision.collider.has_method("hit"):
            collision.collider.hit('moneybag', 2)
            add_collision_exception_with(collision.collider)
        else:
            explode()


func explode():
    var moneybomb_explode_sound_instance = moneybomb_explode_sound.instance()
    moneybomb_explode_sound_instance.connect('finished', moneybomb_explode_sound_instance, 'queue_free')
    get_parent().add_child(moneybomb_explode_sound_instance)

    var coin_spawnpoint = bag_center.global_position
    for coin_target in $coin_targets.get_children():
        var coin_instance = coin.instance()
        var coin_vector = coin_spawnpoint.direction_to(coin_target.global_position).normalized()
        coin_instance.setup(coin_spawnpoint, coin_vector)
        self.get_parent().add_child(coin_instance)

    queue_free()


func _on_moneybomb_lifespan_timer_timeout():
    explode()
