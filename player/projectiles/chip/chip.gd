extends KinematicBody2D

var chip_throw_sound = preload("res://player/projectiles/chip/chip_throw_sound.tscn")

var velocity = Vector2.ZERO
var speed = 300
var lifespan = 0.6
var fire_rate = 0.3

var spawn_position
var vec_to_target


func _ready():
    var timer = Timer.new()
    timer.set_one_shot(true)
    timer.set_wait_time(lifespan)
    timer.connect('timeout', timer, 'queue_free')
    timer.connect('timeout', self, '_on_Timer_end')
    
    add_child(timer)
    timer.start()

    velocity = Vector2(1, 0).rotated(rotation) * speed
    
    var chip_throw_sound_instance = chip_throw_sound.instance()
    chip_throw_sound_instance.connect('finished', chip_throw_sound_instance, 'queue_free')
    get_parent().add_child(chip_throw_sound_instance)


func setup(target, player_position):
    position = player_position
    spawn_position = position

    vec_to_target = target - position
    rotation = vec_to_target.angle()


func _physics_process(delta):
    var collision = move_and_collide(velocity * delta)

    if collision:
        if collision.collider.has_method("hit"):
            collision.collider.hit('casino_chip', 1)
            add_collision_exception_with(collision.collider)
        else:
            velocity = velocity.bounce(collision.normal) 


func _on_Timer_end():
    queue_free()
