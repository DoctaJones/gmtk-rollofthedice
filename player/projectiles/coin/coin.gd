extends KinematicBody2D

var coin_hit_sound = preload('res://player/projectiles/coin/coin_hit_sound.tscn')

var velocity = Vector2.ZERO
var speed = 300
var lifespan = 0.6

func _ready():
    var timer = Timer.new()
    timer.set_one_shot(true)
    timer.set_wait_time(lifespan)
    timer.connect('timeout', timer, 'queue_free')
    timer.connect('timeout', self, '_on_Timer_end')
    
    add_child(timer)
    timer.start()

    velocity = Vector2(1, 0).rotated(rotation) * speed


func setup(spawn_position, vector):
    position = spawn_position
    rotation = vector.angle()


func _physics_process(delta):
    var collision = move_and_collide(velocity * delta)

    if collision:
        if collision.collider.has_method("hit"):
            collision.collider.hit('coin', 1)
            add_collision_exception_with(collision.collider)

        else:
            velocity = velocity.bounce(collision.normal)

            var coin_hit_sound_instance = coin_hit_sound.instance()
            coin_hit_sound_instance.connect('finished', coin_hit_sound_instance, 'queue_free')
            get_parent().add_child(coin_hit_sound_instance)


func _on_Timer_end():
    queue_free()
