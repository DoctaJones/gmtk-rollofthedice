extends KinematicBody2D

onready var animation_player = $AnimationPlayer
onready var movement_indicator = $movement_aimer/movement_indicator
onready var projectile_indicator = $projectile_aimer/projectile_indicator
onready var projectile_spawn = $projectile_aimer/projectile_spawn
onready var projectile_target = $projectile_aimer/projectile_target

var chip = preload("res://player/projectiles/chip/chip.tscn")
var moneybomb = preload("res://player/projectiles/moneybomb/moneybomb.tscn")
var bad_luck_sound = preload("res://player/bad_luck_sound.tscn")

export (int) var speed = 200
export (int) var acceleration = 1000
export (int) var friction = 400
export (Dictionary) var stats = {
    'health': 10,
   }

# Possible player states
enum {
    MOVE,
    ROLLING,
    IDLE
}

var state = IDLE
var velocity = Vector2.ZERO
var roll_multiplier = 1
var bad_luck = false setget set_bad_luck
var good_luck = false setget set_good_luck
var cough_rate = 1
var rng = RandomNumberGenerator.new()

signal player_health_changed
signal player_died
signal player_luck_changed

var current_frame = 0
onready var current_weapon = chip


func _ready():
    var timer = Timer.new()
    timer.name = 'spawn_timer'
    timer.set_wait_time(0.15)
    timer.set_one_shot(true)
    timer.connect('timeout', timer, 'queue_free')
    add_child(timer)
    timer.start()

    rng.randomize()


func _physics_process(delta):
    $movement_aimer.look_at(get_global_mouse_position())
    $projectile_aimer.look_at(get_global_mouse_position())

    match state:
        MOVE: move_state(delta)
        ROLLING: rolling_state()
        IDLE: idle_state()


func get_input():
    if Input.is_action_pressed("roll"):
        state = ROLLING

    if Input.is_action_pressed("shoot") and state == IDLE:
        var target = get_global_mouse_position()
        shoot(projectile_target.global_position, projectile_spawn.global_position)


func move_state(delta):
    if $roll_timer.is_stopped():
        $roll_timer.start()

    projectile_indicator.visible = false
    velocity = velocity.move_toward(Vector2.ZERO, friction * delta)

    var collision = move_and_collide(velocity * delta)

    if collision:
        velocity = velocity.bounce(collision.normal)

    if velocity == Vector2.ZERO:
        state = IDLE
        $roll_timer.stop()
        
        set_weapon($Sprite.frame)
        
        if $Sprite.frame == 0:
            set_bad_luck(true)


func rolling_state():
    if $roll_timer.is_stopped():
        $roll_timer.start()

    var input_vector = Vector2.ZERO
    projectile_indicator.visible = false

    if Input.is_action_pressed("roll"):
        animation_player.play("rolling_in_place")
        movement_indicator.visible = true
        movement_indicator.set_frame(roll_multiplier - 1)

    if Input.is_action_just_released("roll"):
        input_vector = position.direction_to(get_global_mouse_position()).normalized()
        animation_player.stop(true)
        movement_indicator.visible = false
        state = MOVE

    if input_vector != Vector2.ZERO:
        velocity = input_vector * speed * roll_multiplier
        roll_multiplier = 1


func idle_state():
    if bad_luck:
        projectile_indicator.visible = false
    else:
        projectile_indicator.visible = true

    if good_luck:
        good_luck = false

    if not has_node('spawn_timer'):
        get_input()


func roll_die():
    var frame_index

    if bad_luck:
        frame_index = 0
    elif good_luck:
        frame_index = rng.randi_range(2, $Sprite.hframes - 1)
    else:
        frame_index = rng.randi_range(0, $Sprite.hframes - 1)

    $Sprite.set_frame(frame_index)


func set_weapon(index):
    if index in [0,1,2]:
        current_weapon = chip
    else:
        current_weapon = moneybomb
    
    return current_weapon.instance()


func shoot(target, projectile_spawn_position):
    var bullet_timer_wait_time

    if has_node("bullet_timer"):
        pass
    else:
        var timer = Timer.new()
        timer.name = 'bullet_timer'
        timer.set_one_shot(true)
        timer.connect('timeout', timer, 'queue_free')
    
        if bad_luck:
            var bad_luck_sound_instance = bad_luck_sound.instance()
            bad_luck_sound_instance.connect("finished", bad_luck_sound_instance, 'queue_free')
            get_parent().add_child(bad_luck_sound_instance)

            bullet_timer_wait_time = cough_rate
        else:
            var c = set_weapon($Sprite.frame)
            c.setup(target, projectile_spawn_position)
            self.get_parent().add_child(c)

            bullet_timer_wait_time = c.fire_rate

        timer.set_wait_time(bullet_timer_wait_time)
        add_child(timer)
        timer.start()


func hit(type, damage):
    if type == 'contact' and has_node('contact_damage_invincible'):
        damage = 0
    else:
        contact_damage_buffer()

    stats['health'] = stats['health'] - damage
    emit_signal("player_health_changed")

    if stats['health'] <= 0:
        emit_signal("player_died")
        die()


func contact_damage_buffer():
    var timer = Timer.new()
    timer.name = 'contact_damage_invincible'
    timer.set_one_shot(true)
    timer.set_wait_time(0.15)
    timer.connect('timeout', timer, 'queue_free')

    add_child(timer)
    timer.start()


func set_bad_luck(luck):
    bad_luck = luck
    emit_signal('player_luck_changed', bad_luck)
    
    if not bad_luck:
        if has_node('bullet_timer'):
            get_node('bullet_timer').queue_free()

func set_good_luck(luck):
    good_luck = luck


func spawn(spawn_position):
    position = spawn_position


func die():
    queue_free()


func _on_AnimationPlayer_animation_finished(anim_name):
    if roll_multiplier < 3:
        roll_multiplier = roll_multiplier + 1


func _on_roll_timer_timeout():
    roll_die()
